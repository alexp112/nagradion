<?php

class Figure {
    protected $isBlack;

    public function __construct($isBlack) {
        $this->isBlack = $isBlack;
    }

    /** @noinspection PhpToStringReturnInspection */
    public function __toString() {
        throw new \Exception("Not implemented");
    }

    /**
     * Возвращает черная ли фигура
     *
     * @return bool
     */
    public function getIsBlack():bool
    {
        return $this->isBlack;
    }

    /**
     * Проверяет может ли фигура ходить данным образом
     *
     * @param $xFrom
     * @param $yFrom
     * @param $xTo
     * @param $yTo
     * @param array $figures
     * @return bool
     */
    public function isMoveValid($xFrom, $yFrom, $xTo, $yTo, array $figures): bool
    {
        return true;
    }

    /**
     * Проверяет может ли фигура срубить другую
     *
     * @param $xFrom
     * @param $yFrom
     * @param $xTo
     * @param $yTo
     * @return bool
     */
    public function canCapture($xFrom, $yFrom, $xTo, $yTo): bool
    {
        return true;
    }
}
