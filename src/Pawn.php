<?php

class Pawn extends Figure {
    public function __toString() {
        return $this->isBlack ? '♟' : '♙';
    }

    /**
     * Проверяет может ли фигура ходить данным образом
     *
     * @param $xFrom
     * @param $yFrom
     * @param $xTo
     * @param $yTo
     * @param array $figures
     * @return bool
     */
    public function isMoveValid($xFrom, $yFrom, $xTo, $yTo, array $figures): bool
    {
        $xFromValue = Desk::X_VALUES[$xFrom];
        $xToValue = Desk::X_VALUES[$xTo];

        // проверяем, что нельзя ходить назад
        if ($this->isBlack && $yFrom < $yTo) {
            return false;
        }
        if (!$this->isBlack && $yFrom > $yTo) {
            return false;
        }

        // проверяем, что нельзя ходить по горизонтали
        if ($yFrom == $yTo) {
            return false;
        }

        // вперед можно ходить только на 1 клетку
        $moveForwardCellsNum = 1;
        // если первый ход - можно походить на 2 клетки вперед
        if ($yFrom == 7 || $yFrom == 2) {
            $moveForwardCellsNum = 2;
        }

        // Если ход по вертикали и на большее кол-во клеток, чем возможно
        if ($xFrom == $xTo && abs($yFrom - $yTo) > $moveForwardCellsNum) {
            return false;
        }

        // Фигура, которая находится на клетке, на которую ходим (если есть)
        $toFigure = isset($figures[$xTo][$yTo]) ? $figures[$xTo][$yTo] : null;
        // если на клетке, куда ходим есть фигура
        if ($toFigure !== null) {
            // если ходим по вертикали
            if ($xFrom == $xTo) {
                return false;
            }
            // Если ход не по диагонали
            if (abs($xFromValue - $xToValue) > 1
                && abs($yFrom - $yTo) > 1) {
                return false;
            }
        } else {
            // Если ход по диагонали
            if (abs($xFromValue - $xToValue) == 1
                && abs($yFrom - $yTo) == 1) {
                return false;
            }
        }

        // если первый ход
        if ($yFrom == 7 || $yFrom == 2) {
            // и пешка ходит на 2 клетки
            if (abs($yFrom - $yTo) == 2) {
                // и на её пути есть другая фигура
                if (($yTo > $yFrom && isset($figures[$xTo][$yTo-1]))
                    ||($yTo < $yFrom && isset($figures[$xTo][$yTo+1]))) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Проверяет может ли фигура срубить другую
     *
     * @param $xFrom
     * @param $yFrom
     * @param $xTo
     * @param $yTo
     * @return bool
     */
    public function canCapture($xFrom, $yFrom, $xTo, $yTo): bool
    {
        $xFromValue = Desk::X_VALUES[$xFrom];
        $xToValue = Desk::X_VALUES[$xTo];

        // Если ход по диагонали
        if (abs($xFromValue - $xToValue) == 1
            && abs($yFrom - $yTo) == 1) {
            return true;
        }

        return false;
    }
}
