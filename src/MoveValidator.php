<?php

/**
 * Валидатор ходов, проверяет может ли фигура походить данным образом
 */
class MoveValidator
{
    public function __construct()
    {
    }

    /**
     * Проверяет может ли фигура ходить данным образом
     *
     * @param array $figures
     * @param $xFrom
     * @param $yFrom
     * @param $xTo
     * @param $yTo
     * @return bool
     */
    public function isMoveValid(array $figures, $xFrom, $yFrom, $xTo, $yTo): bool
    {
        /** @var Figure $figure */
        $figure = $figures[$xFrom][$yFrom];

        // Может ли фигура в принципе так ходить
        if (!$figure->isMoveValid($xFrom, $yFrom, $xTo, $yTo, $figures)) {
            return false;
        }

        // Если клетка, на которую ходим - занята - проверяем можем ли срубить
        if (isset($figures[$xTo][$yTo])) {
            if (!$figure->canCapture($xFrom, $yFrom, $xTo, $yTo)) {
                return false;
            }
        }

        return true;
    }
}
